#pragma once
#ifndef PROCESSING_H
#define PROCESSING_H

#include "marathon.h"

int process(marathon* array[], int size);

#endif
