#include "pch.h"
#include "CppUnitTest.h"
#include "../main-project/marathon.h"
#include "../main-project/processing.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unittestproject
{
	marathon* build_finish(int hours, int minutes, int seconds)
	{
		marathon* elem = new marathon;
		elem->finish.Hour = hours;
		elem->finish.Minute = minutes;
		elem->finish.Seconds = seconds;
		return elem;
	}

	void delete_finish(marathon* array[], int size)
	{
		for (int i = 0; i < size; i++)
		{
			delete array[i];
		}
	}




	TEST_CLASS(unittestproject)
	{
	public:
		TEST_METHOD(TestMethod1)
		{
			marathon* elems[2];
			elems[0] = build_finish(10, 1, 48);
			elems[1] = build_finish(19, 2, 59);
			Assert::AreEqual(10, process(elems, 2));
			delete_finish(elems, 2);
		}

		TEST_METHOD(TestMethod2)
		{
			marathon* elems[2];
			elems[0] = build_finish(7, 41, 52);
			elems[1] = build_finish(14, 35, 1);
			Assert::AreEqual(7, process(elems, 2));
			delete_finish(elems, 2);
		}

		TEST_METHOD(TestMethod3)
		{
			marathon* elems[2];
			elems[0] = build_finish(15, 47, 49);
			elems[1] = build_finish(22, 0, 29);
			Assert::AreEqual(15, process(elems, 2));
			delete_finish(elems, 2);
		}

		TEST_METHOD(TestMethod4)
		{
			marathon* elems[2];
			elems[0] = build_finish(4, 4, 4);
			elems[1] = build_finish(4, 4, 4);
			Assert::AreEqual(4, process(elems, 2));
			delete_finish(elems, 2);
		}

		TEST_METHOD(TestMethod5)
		{
			marathon* elems[2];
			elems[0] = build_finish(22, 15, 6);
			elems[1] = build_finish(23, 59, 59);
			Assert::AreEqual(22, process(elems, 2));
			delete_finish(elems, 2);
		}
	};
}
